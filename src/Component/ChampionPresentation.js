import { Paper, Avatar, Chip } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from "react-router";
import Stat  from '../Component/tableStat'

export default function ChampionPresentation (){
    let params = useParams();
    const linkImg = "http://ddragon.leagueoflegends.com/cdn/11.24.1/img/champion/" + params.champName +".png";
    const linkdata = "http://ddragon.leagueoflegends.com/cdn/11.24.1/data/en_US/champion/"+params.champName+".json";
    const linkBackgroundImg = "https://blitz-cdn.blitz.gg/blitz/centered/"+ params.champName +"_Splash_Centered_0.jpg";
    const [champ,setChamp] = useState([]);
    const [stats,setStats] = useState({});

    let isMounted = true;

    const getData= () =>{
        fetch(linkdata
    ).then((response)=>
        response.json()
    ).then((data)=>{
        setChamp(data.data[params.champName]);
    });
}
    useEffect( () => {
        if (isMounted){
            getData()
            isMounted = false;
            //paperTable.innerHTML(<Stat tableInformation={champ}/>);
        }
    }, isMounted)

    const tags = champ.tags;
    console.log(tags);
    const listTag = tags?.map((tag) => (
    <Chip
        avatar={<Avatar alt={champ.id} src={`https://www.metasrc.com/assets/v/5.0.1/images/tags/${tag.toLowerCase()}_icon.png`} variant="rounded"/>}
        label={tag}
        variant="filled"
        color="secondary"
        key={tag}
    />));
    const stat = champ['info'];

        console.log(champ.id);
        console.log(listTag);
        console.log(champ);
        console.log(champ.stats);
        //setStats(Object.values(champ.stats));
        console.log(stat);

    return(
        <div className="SummonerPresentationContainer">
            <div className="RowContainer">
                <Paper variant="outlined" elevation={3} className="PaperCard">
                    <div className="leftPart">
                        <Avatar alt={params.Name} src={linkImg} sx={{ width: 80, height: 80 }}/>
                    </div>
                    <div className="rightPart">
                        <h2>{champ.id}</h2>
                    </div>
                    <div className="chipline">
                        {listTag}
                    </div>
                </Paper>
                <Paper variant="outlined" elevation={3} className="PaperCardMMR">
                    <h4>{champ.title}</h4>
                    <p>{champ.lore}</p>
                    <br />
                </Paper>
            </div>
            <div className="RowContainer">
                <Paper variant="outlined" elevation={3} className="PaperCardMMR">
                {/*!isMounted
                    ? <p>test</p>
                    : <Stat tableInformation={champ['stats']}/>
                */}
                <Stat championName={params.champName}/>
                </Paper>
            </div>
        </div>
    );
  }
  