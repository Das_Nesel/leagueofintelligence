import { Card,CardMedia, CardContent, Typography, CardActions, Button } from '@mui/material';
import { useParams } from "react-router";
import { useEffect, useState } from 'react';
import championSuggestion from "../json/championSuggestion.json";

export default function Suggestion (){
    let params = useParams();
    //const champions = championSuggestion[params.Name].Suggestion;
    //console.log(champions.Suggestion);

    const linkdata = `http://127.0.0.1:8000/getuserscore/${params.champName}`;
    const [champ,setChamp] = useState([]);

    let isMounted = true;

    const getData= () =>{
        fetch(linkdata
    ).then((response)=>
        response.json()
    ).then((data)=>{
        setChamp(data[params.champName]);
    });
}
    useEffect( () => {
        if (isMounted){
            getData()
            isMounted = false;
        }
    }, isMounted)

    const listChampion= champ?.map((champion) => (
        <Card sx={{ maxWidth: 345 }}>
            <CardMedia
                component="img"
                height="140"
                image={`https://blitz-cdn.blitz.gg/blitz/centered/${champion}_Splash_Centered_0.jpg`}
                alt={champion}
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                {champion}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                Lizards are a widespread group of squamate reptiles, with over 6,000
                species, ranging across all continents except Antarctica
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="secondary" href={`/champ/${champion}`}>Learn More About {champion}</Button>
            </CardActions>
          </Card>
    ));

  return(
      <div className="SuggestionContainer">
          {listChampion}
      </div>
  );
}