import { useEffect, useState } from "react";
import { useParams } from "react-router";

export default function Champion (){
    let params = useParams();
    const linkImg = "http://ddragon.leagueoflegends.com/cdn/11.23.1/img/champion/" + params.champName +".png";
    const linkdata = "http://ddragon.leagueoflegends.com/cdn/11.23.1/data/en_US/champion/"+params.champName+".json";
    const linkBackgroundImg = "https://blitz-cdn.blitz.gg/blitz/centered/"+ params.champName +"_Splash_Centered_0.jpg";

    const [champ,setChamp] = useState([]);

    let isMounted = true;

    const getData= () =>{
        fetch(linkdata
    ).then((response)=>
        response.json()
    ).then((data)=>{
        setChamp(data.data[params.champName]);
    });
}
    useEffect( () => {
        if (isMounted){
            getData()
            isMounted = false;
        }
    }, isMounted)

    const tags = champ.tags;
    console.log(tags);
    const listTag = tags?.map((tag) => (<li>{tag}</li>));

  return(
      <div className="championPictureContainer">
        <div className="pictureChamp">
            <img src={linkBackgroundImg} />
        </div>
        <div className="championContainer">
            <div className="titleRaw">
                <div className="imgColum">
                    <img src={linkImg} className="centerImg"/>
                </div>
                <div className="nameRawCol">
                    <h2 >{params.champName}</h2>
                    <h3 >{champ.title}</h3>
                </div>
                <div className="titleRowCol">
                    <p>
                        {champ.blurb}
                    </p>
                </div>
            </div>
            <div className="shouldPlay">
                    <h3>You should play this champ as:</h3>
                    <ul>
                        {listTag}
                    </ul>
            </div>
          </div>
      </div>
  );
}