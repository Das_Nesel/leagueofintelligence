import { Table, TableBody, TableHead, TableRow, TableCell } from '@mui/material';
import champion from '../json/champion.json'

export default function tableStat (props){

    console.log(props.championName)

    const champ = champion.data[props.championName];
    const stat = champ.stats;
    console.log(champ);
    console.log(stat);
  return(
    <Table>
        <TableHead>
            <TableRow>
                <TableCell>Stats</TableCell>
                <TableCell>Start</TableCell>
                <TableCell>Per level</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            <TableRow>
                <TableCell>hp</TableCell>
                <TableCell>{stat.hp}</TableCell>
                <TableCell>{stat.hpperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>hp regeneration</TableCell>
                <TableCell>{stat.hpregen}</TableCell>
                <TableCell>{stat.hpregenperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>mana point</TableCell>
                <TableCell>{stat.mp}</TableCell>
                <TableCell>{stat.mpperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>mana regeneration</TableCell>
                <TableCell>{stat.mpregen}</TableCell>
                <TableCell>{stat.mpregenperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>attack damage</TableCell>
                <TableCell>{stat.attackdamage}</TableCell>
                <TableCell>{stat.attackdamageperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>attack speed</TableCell>
                <TableCell>{stat.attackspeed}</TableCell>
                <TableCell>{stat.attackspeedperlevel}%</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>armor</TableCell>
                <TableCell>{stat.armor}</TableCell>
                <TableCell>{stat.armorperlevel}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell>spellblock</TableCell>
                <TableCell>{stat.spellblock}</TableCell>
                <TableCell>{stat.spellblockperlevel}</TableCell>
            </TableRow>
        </TableBody>
    </Table>
  );
}