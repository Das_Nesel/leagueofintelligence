import { AppBar, Toolbar, Button } from '@mui/material';

export default function NavBar (){

    return(
        <div>
          <AppBar color="secondary" position="relative">
            <Toolbar variant="dense" >
                <Button variant="contained" href="/">
                    Home
                </Button>
            </Toolbar>
          </AppBar>
        </div>
    );
  }
  