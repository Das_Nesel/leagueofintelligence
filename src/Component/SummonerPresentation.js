import { Paper, Avatar, Chip, Button } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from "react-router";
import Summoner from "../test.json";

export default function SummonerPresentation (){
    let params = useParams();
    const linkAPIMMR = `http://${params.server}.whatismymmr.com/api/v1/summoner?name=${params.Name}`;
    console.log(linkAPIMMR);
    const [MMR, setMMR] = useState({});
    const [summoner, setSummoner] = useState([]);
    const [information, setInformation] = useState([]);
    const [rank, setRank] = useState([]);

    let isMounted = 0;

    const getSummonerData = () =>{
        console.log(params.server);
        let server = "kr";
        if (params.server == "EUW") {
            server = "euw1";
        }
        const linkData = `https://${server}.api.riotgames.com/lol/summoner/v4/summoners/by-name/${params.Name}?api_key=RGAPI-08d0792d-afba-4444-a181-6d0f6251481b`;
        fetch(linkData
        ).then((response)=>
        response.json()
        ).then((data)=>{
            setSummoner(data);
            console.log(summoner);
        });
    }

    const getRank = () =>{
        console.log(params.server);
        let server = "kr";
        if (params.server == "EUW") {
            server = "euw1";
        }
        const linkData = `https://${server}.api.riotgames.com/lol/league/v4/entries/by-summoner/${summoner.id}?api_key=RGAPI-08d0792d-afba-4444-a181-6d0f6251481b`;
        fetch(linkData
        ).then((response)=>
        response.json()
        ).then((data)=>{
            setInformation(data);
            console.log(information);
            setRank(information[0]["tier"]);
            console.log(information);
        });
    }
    
    useEffect( () => {
        if (isMounted==0) {
            if (localStorage.getItem("MMR") === null) {
                console.log("test");
                localStorage.setItem('MMR', JSON.stringify({
                    "Ranked": "unavailable",
                    "Normal": "unavailable"
                }));
                setMMR(JSON.parse(localStorage.getItem('MMR')));
            }else{
                setMMR(JSON.parse(localStorage.getItem('MMR')));
            }
            getSummonerData();
            isMounted = 1;
        }
        if (isMounted==1) {
            getRank();
            console.log(information);
            isMounted = 2;
        }
    }, isMounted);

    const MMRInit = () =>{
        fetch(`https://euw.whatismymmr.com/api/v1/summoner?name=${params.Name}`,
        {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': 'http://euw.whatismymmr.com'
        },
   }).then((response)=>
                response.json()
            ).then((data)=>{
                const dataMMR = data;
                console.log(dataMMR);
                if (dataMMR) {
                    let rankedMMR = dataMMR.ranked.avg;
                    let normalMMR = dataMMR.normal.avg;
                    if (!rankedMMR) {
                        rankedMMR = "unavailable";
                    }
                    if (!normalMMR) {
                        normalMMR = "unavailable";
                    }
                    localStorage.setItem('MMR', JSON.stringify({
                        "Ranked": rankedMMR,
                        "Normal": normalMMR
                    }));
                }
                console.log(data);
            });
            setMMR(JSON.parse(localStorage.getItem('MMR')));
    };

    const Refresh = () =>{
        MMRInit();
    };
    
    const linkSummonerImg = `http://ddragon.leagueoflegends.com/cdn/11.24.1/img/profileicon/${summoner.profileIconId}.png`;
    const linkRankImg = `/LOL_RANK/Emblem_${rank}.png`;

    return(
        <div className="SummonerPresentationContainer">
            <div className="RowContainer">
                <Paper variant="outlined" elevation={3} className="PaperCard">
                    <div className="leftPart">
                        <Avatar alt={summoner.name} src={linkSummonerImg} sx={{ width: 80, height: 80 }}/>
                    </div>
                    <div className="rightPart">
                        <h2>{summoner.name}</h2>
                    </div>
                    <div className="chipline">
                        <Chip
                            avatar={<Avatar alt="Summoner.rank" src={linkRankImg} />}
                            label={rank}
                            variant="filled"
                        />
                        <Chip label={"Level: "+ summoner.summonerLevel} variant="filled" color="secondary" />
                    </div>
                </Paper>
                <Paper variant="outlined" elevation={3} className="PaperCardMMR">
                    <h4>Estimation Ranked MMR: {MMR.Ranked}</h4>
                    <h4>Estimation Normal MMR: {MMR.Normal}</h4>
                    <br />
                    <Button variant="contained" color="secondary" fullWidth="false" size="medium" onClick={Refresh}>Refresh</Button>
                </Paper>
            </div>
            
        </div>
    );
  }
  