import welcome_png from '../img/welcom_png.png';
import { useNavigate } from "react-router-dom";

export default function HomePage (){
  
  let navigate = useNavigate();

  function pathDirection() {
    const server = document.getElementById('serverSelect');
    const summonerName = document.getElementById('pseudoSearch');
    const linkToChamp = "/" + server.value + "/summoner/" + summonerName.value;
    console.log(linkToChamp);
    navigate(linkToChamp);
  }

  return(
      <div>
        <div className="App-header">
          <img src={welcome_png} className="App-logo" alt="logo" />
          <h1 className="WelcomeTitle">League of intelligence</h1>
          <div className="pseudoSearchRow">
            <input type="text" id="pseudoSearch" className="pseudoSearch" placeholder="Summoner Name" autocomplete="off"/>
            <select className="serverSelect" id="serverSelect" name="serverRegion">
              <option value="KR">KR</option>
              <option value="EUW">EUW</option>
            </select>
            <button className="buttonSearch" onClick={pathDirection}>GO!</button>
          </div>
        </div>
      </div>
  );
}
