import NavBar from '../Component/navBar';
import ChampionPresentation from '../Component/ChampionPresentation';

export default function ChampionPage (){

  return(
      <div className="SummonerPage">
        <NavBar/>
        <ChampionPresentation />
      </div>
  );
}