import NavBar from '../Component/navBar';
import Suggestion from '../Component/Suggestion';
import SummonerPresentation from '../Component/SummonerPresentation';

export default function SummonerPage (){

  return(
      <div className="SummonerPage">
        <NavBar/>
        <SummonerPresentation />
        <Suggestion/>
      </div>
  );
}