import Homepage from './Page/HomePage';
import Champion from './Component/Champion';
import ChamPage from './Page/ChamPage';
import SummonerPage from './Page/SummonerPage';
import ChampionPage from './Page/ChampionPage';
import './App.css';
import { BrowserRouter as Router,  Switch,  Route,  Link, Routes} from "react-router-dom";

function App() {
  return (
    <div className="App">
     <Router>
        <Routes>
          <Route path="/">
            <Route index element={<Homepage />}/>
            <Route path="champ" element={<ChamPage />}/>
            <Route path="champ/:champName" element={<ChampionPage />} />
            <Route path=":server/summoner/:Name" element={<SummonerPage />}>
            </Route>
          </Route>
        </Routes>
      </Router>
    </div>
  );
  
}

export default App;
